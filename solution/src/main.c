#include "bmp_handlers/bmp_reader.h"
#include "bmp_handlers/bmp_rotator.h"
#include "bmp_handlers/bmp_writer.h"
#include "file_handlers/file_closer.h"
#include "file_handlers/file_opener.h"

#include <stdio.h>
#include <stdlib.h>

#define COUNT_OF_ARGS 3
#define ERR_FINISH_CODE 1

void print_error_msg(char* err_message) {
    fprintf(stderr, "%s", err_message);
}

int main( int argc, char** argv ) {
    if (argc != COUNT_OF_ARGS) {
        print_error_msg("Incorrect number of args!");
        return ERR_FINISH_CODE;
    }
    const char* input_filename = argv[1];
    const char* output_filename = argv[2];
    FILE *input_file, *output_file;

    enum open_status inputOpenStatus = open_file(&input_file, input_filename, "rb");
    enum open_status outputOpenStatus = open_file(&output_file, output_filename, "wb");
    if (inputOpenStatus == ERROR_OPEN) {
        print_error_msg("Input file is incorrect or doesn't exist!\n");
        return ERR_FINISH_CODE;
    }
    if (outputOpenStatus == ERROR_OPEN) {
        print_error_msg("Output file is incorrect or doesn't exist!\n");
        return ERR_FINISH_CODE;
    }

    struct image* img = malloc(IMAGE_STRUCT_SIZE);
    enum read_status readStatus = from_bmp(input_file, img);

    // readStatus 'READ_OK' = 0
    if (readStatus) {
        close_file(input_file);
        close_file(output_file);
        free(img -> data);
        print_error_msg("Can't read bmp image!");
        return ERR_FINISH_CODE;
    }

    struct image rotated_img = rotate(*img);
    free(img->data);
    free(img);

    to_bmp(output_file, &rotated_img);
    free(rotated_img.data);

    fclose(input_file);
    fclose(output_file);
    return 0;
}
