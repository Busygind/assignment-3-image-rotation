//
// Created by dmitryb on 11/16/22.
//

#include "file_handlers/file_closer.h"

enum close_status close_file(FILE* file) {
    if (!file) {
        return ERROR_CLOSE;
    }
    if (fclose(file)) {
        return ERROR_CLOSE;
    }
    return SUCCESS_CLOSE;
}
