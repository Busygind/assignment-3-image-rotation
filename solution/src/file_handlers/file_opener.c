//
// Created by dmitryb on 11/16/22.
//

#include "file_handlers/file_opener.h"

enum open_status open_file(FILE** file, const char* img_filename, const char* read_mode) {
    if (!file || !img_filename || !read_mode) {
        return ERROR_OPEN;
    }
    *file = fopen(img_filename, read_mode);

    if (!*file) {
        return ERROR_OPEN;
    }
    return SUCCESS_OPEN;
}
