//
// Created by dmitryb on 11/16/22.
//

#include "bmp_handlers/bmp_rotator.h"
#include <stdlib.h>

struct image rotate(struct image const source) {
    struct image out;
    out.width = source.height;
    out.height = source.width;
    out.data = malloc(PIXEL_STRUCT_SIZE * out.width * out.height);
    uint64_t row = 0;
    uint64_t count = 0;
    while (row < out.height) {
        for (uint64_t i = out.height * out.width ; i > 0; i--) {
            if ((i - 1) % out.height == row) {
                *(out.data + count) = *(source.data + i - 1);
                count++;
            }
        }
        row++;
    }
    return out;
}
