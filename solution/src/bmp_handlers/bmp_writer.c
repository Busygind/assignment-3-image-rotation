//
// Created by dmitryb on 11/16/22.
//

#include "bmp_handlers/bmp_writer.h"
#include "bmp_handlers/bmp_header.h"
#include <stdio.h>
#include <stdlib.h>

#define NUM_OF_BITS 24
#define PIXEL_SIZE 4
#define FILE_TYPE 19778
#define ALL_HEADERS_SIZE 54
#define INFO_HEADERS_SIZE 40
#define PLANES 1
#define BIT_COUNT 24
#define COMPRESSION 0
#define X_PIXELS_PER_METER 2834
#define Y_PIXELS_PER_METER 2834
#define RESERVED 0

enum write_status to_bmp( FILE* out, struct image const* img) {
    int padding = (PIXEL_OFFSET - (int)((PIXEL_STRUCT_SIZE * img->width) % PIXEL_OFFSET)) % PIXEL_OFFSET;
    struct bmp_header* header = malloc(BMP_HEADER_STRUCT_SIZE);

    // set fields
    header->bfType = FILE_TYPE;
    header->bfileSize = img->height * img->width * PIXEL_STRUCT_SIZE + img->height * padding + ALL_HEADERS_SIZE;
    header->bfReserved = RESERVED;
    header->bOffBits = ALL_HEADERS_SIZE;
    header->biSize = INFO_HEADERS_SIZE;
    header->biWidth = img->width;
    header->biHeight = img->height;
    header->biPlanes = PLANES;
    header->biBitCount = BIT_COUNT;
    header->biCompression = COMPRESSION;
    header->biSizeImage = img->height * img->width;
    header->biXPelsPerMeter = X_PIXELS_PER_METER;
    header->biYPelsPerMeter = Y_PIXELS_PER_METER;
    header->biClrUsed = 0;
    header->biClrImportant = 0;

    uint64_t write_status = fwrite(header, BMP_HEADER_STRUCT_SIZE, 1, out);
    free(header);

    if (!write_status) {
        return WRITE_ERROR;
    }

    int pixel_counter = 0;
    for (size_t i = 0; i < img->height; i++) {
        for (size_t j = 0; j < img->width; j++) {
            write_status = fwrite(img->data + pixel_counter, PIXEL_STRUCT_SIZE, 1, out);
            if (!write_status) {
                return WRITE_ERROR;
            }
            pixel_counter++;
        }

        fseek(out, (int8_t) padding, SEEK_CUR);
    }
    return WRITE_OK;
}
