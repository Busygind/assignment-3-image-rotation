//
// Created by dmitryb on 11/9/22.
//

#include "bmp_handlers/bmp_reader.h"
#include "bmp_handlers/bmp_header.h"
#include <stdio.h>
#include <stdlib.h>

#define NUM_OF_READING_STRUCTS 1
#define NUM_OF_PIXEL_ARGS 3

enum read_status from_bmp( FILE* in, struct image* img ) {
    if (in == NULL) {
        return INPUT_FILE_IS_NULL;
    }

    struct bmp_header header;
    size_t read_status = fread(&header, BMP_HEADER_STRUCT_SIZE, NUM_OF_READING_STRUCTS, in);
    if (!read_status) {
        return READ_INVALID_HEADER;
    }

    uint32_t img_width = header.biWidth;
    uint32_t img_height = header.biHeight;
    img->data = malloc(PIXEL_STRUCT_SIZE * img_height * img_width);
    img->width = img_width;
    img->height = img_height;

    int padding = (PIXEL_OFFSET - (int)((PIXEL_STRUCT_SIZE * img_width) % PIXEL_OFFSET)) % PIXEL_OFFSET;
    fseek(in, header.bOffBits, SEEK_SET);

    for (uint32_t i = 0; i < img_height; i++) {
        read_status = fread(img->data + img_width * i, PIXEL_STRUCT_SIZE, img_width, in);
        if (!read_status) {
            return READ_INVALID_BITS;
        }
        fseek(in, padding, SEEK_CUR);
    }

    return READ_OK;
}
