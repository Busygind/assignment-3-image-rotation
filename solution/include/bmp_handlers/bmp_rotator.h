//
// Created by dmitryb on 11/16/22.
//

#include "../image_struct.h"

#ifndef IMAGE_TRANSFORMER_BMP_ROTATOR_H
#define IMAGE_TRANSFORMER_BMP_ROTATOR_H

struct image rotate(struct image const source);

#endif //IMAGE_TRANSFORMER_BMP_ROTATOR_H
