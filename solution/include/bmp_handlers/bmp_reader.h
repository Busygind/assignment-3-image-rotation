#include "../image_struct.h"
#include <stdio.h>

#ifndef IMAGE_TRANSFORMER_BMP_READER_H
#define IMAGE_TRANSFORMER_BMP_READER_H

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_BITS = 1,
    READ_INVALID_HEADER = 2,
    INPUT_FILE_IS_NULL = 3
};

enum read_status from_bmp( FILE* in, struct image* img );

#endif
