//
// Created by dmitryb on 11/9/22.
//
#ifndef IMG_TRANSFORMER_BMP_STRUCT
#define IMG_TRANSFORMER_BMP_STRUCT

#define BMP_HEADER_STRUCT_SIZE sizeof(struct bmp_header)
#define PIXEL_OFFSET 4

#include  <stdint.h>

struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} __attribute__((packed));

#endif


