//
// Created by dmitryb on 11/16/22.
//

#include "image_struct.h"
#include <stdio.h>

#ifndef IMAGE_TRANSFORMER_BMP_WRITER_H
#define IMAGE_TRANSFORMER_BMP_WRITER_H

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR = 1

};

enum write_status to_bmp( FILE* out, struct image const* img );

#endif //IMAGE_TRANSFORMER_BMP_WRITER_H
