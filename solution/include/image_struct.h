#ifndef IMG_STRUCT
#define IMG_STRUCT

#include <inttypes.h>

#define PIXEL_STRUCT_SIZE sizeof(struct pixel)
#define IMAGE_STRUCT_SIZE sizeof(struct image)

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

#endif
