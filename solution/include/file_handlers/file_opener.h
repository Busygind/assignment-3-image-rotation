//
// Created by dmitryb on 11/16/22.
//

#include <stdio.h>

#ifndef IMAGE_TRANSFORMER_FILE_OPENER_H
#define IMAGE_TRANSFORMER_FILE_OPENER_H

enum open_status {
    SUCCESS_OPEN,
    ERROR_OPEN
};

enum open_status open_file(FILE** file, const char* img_filename, const char* read_mode);

#endif //IMAGE_TRANSFORMER_FILE_OPENER_H
