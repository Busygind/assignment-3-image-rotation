//
// Created by dmitryb on 11/16/22.
//

#include <stdio.h>

#ifndef IMAGE_TRANSFORMER_FILE_CLOSER_H
#define IMAGE_TRANSFORMER_FILE_CLOSER_H

enum close_status {
    SUCCESS_CLOSE,
    ERROR_CLOSE
};

enum close_status close_file(FILE* file);

#endif //IMAGE_TRANSFORMER_FILE_CLOSER_H
